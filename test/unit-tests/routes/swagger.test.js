'use strict'

const request = require('supertest')
const app = require('../../../app')

describe('swagger route', () => {
  describe('/swagger.json', () => {
    test('should respond with json', () => {
      return request(app)
        .get('/swagger.json')
        .set('Accept', 'application/json')
        .expect(200)
        .expect('Content-Type', /json/)
    })
  })

  describe('/api/docs', () => {
    test('should respond with json', () => {
      return request(app)
        .get('/api/docs')
        .expect(200)
        .expect('Content-Type', /text\/html; charset=UTF-8/)
    })
  })
})
