'use strict'

const request = require('supertest')
const app = require('../../../app')

describe('healthz route', () => {
  describe('/healthz', () => {
    test('should respond with json', () => {
      return request(app)
        .get('/api/v1/healthz')
        .set('Accept', 'application/json')
        .expect(200)
        .expect('Content-Type', /json/)
    })
  })
})
