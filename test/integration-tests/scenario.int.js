'use strict'

const fetch = require('node-fetch')

describe('fetch endpoint', () => {
  test('should launch back a Boom error', async () => {
    const res = await fetch('http://localhost:3000/api/v1/users/')
    expect(res.status).toEqual(200)
  })
})
