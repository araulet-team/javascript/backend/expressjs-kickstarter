# ExpressJS Boilerplate Starter Kit [![pipeline status](https://gitlab.com/araulet-team/javascript/backend/expressjs-kickstarter/badges/master/pipeline.svg)](https://gitlab.com/araulet-team/javascript/libs/template/commits/master) [![coverage report](https://gitlab.com/araulet-team/javascript/backend/expressjs-kickstarter/badges/master/coverage.svg)](https://gitlab.com/araulet-team/javascript/libs/template/commits/master) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com) [![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

Simple way to kickstart your Nodejs microservice project with every tools already setup for you.

## Table of Contents

* [Features](#features)
* [Getting Started](#getting-started)
* [Commands](#commands)
* [Test](#test)
* [Advanced](#advanced)
* [Licence](#licence)
* [Thanks](#thanks)

## Features

All tools includes and ready to go.

* `Docker`
    - **Local development** with Docker and [debug mode](https://nodejs.org/en/docs/inspector) configured.
    - Optimize image for production (multistaging)
* `CI/CD`
    - build production ready images
    - tests (unit/integration)
    - code quality check
    - security check
    - commit version bumps on master
* `Best Practices`
    - Docker
    - NodeJS
    - Expressjs
* `Application`
    - Pre-configured Joi validation
    - Easy way to load configuration per environment
    - Loggers made easy with Winston
    - JSDoc / Code Coverage / StandardJS
    - Swagger

## Getting Started

```shell
$ docker-compose up --build
```

API available [here](localhost/api/docs).

## Debug

When working in local you have access to node inspector on port `9229`.

* Open `chrome://inspect/#devices` in a Chromium-based browser.
* In `Remote Target` you should see your application
* Click on `Open dedicated DevTools for Node`
* Click on `sources` and `node`
* Your application files are there

## Tests

#### unit-tests

```shell
$ docker-compose up --build --detach 
$ docker-compose exec -T app npm test
$ docker-compose down 
```

#### integration-tests
```shell
$ docker-compose up --build --detach
$ docker-compose exec -T app npm run test:int
$ docker-compose down 
```
## Advanced

### Application Configuration

All configuration file are located in `config` folder and use a wrapper of `nconf` module provided by [config-loader](https://gitlab.com/araulet-team/javascript/libs/config-loader).

### Application Routes Configuration

This boilerplate is using [expressjs-routes-loader](https://gitlab.com/araulet-team/javascript/libs/expressjs-routes-loader). See the documentation for more example.

### Commands

**warning**: You should always execute these command inside your docker

* `npm test`: run jest with code coverage report for unit testing
* `npm test:int`: run jest with code coverage report for integration testing
* `npm opn test`: open code coverage report
* `npm run generate-doc`: generate jsdoc
* `npm run lint`: run standardJS linter
* `npm run lint:fix`: run standardJS and fix

**Notes**: cross-environment plugins are available and use npm run script:
* [opn](https://github.com/sindresorhus/opn)
* [cross-env](https://github.com/kentcdodds/cross-env)

## Thanks

* [ExpressJS](https://expressjs.com/)
* [Joi](https://github.com/hapijs/joi)
* [StandardJS](https://standardjs.com/)
* [Jest](https://facebook.github.io/jest/) 
* [Docker](https://www.docker.com/)
* [JSDoc](http://usejsdoc.org/)
* [Nodemon](https://nodemon.io/)
* [Morgan](https://github.com/expressjs/morgan)
* [Winston](https://github.com/winstonjs/winston/tree/2.x)
* [EditorConfig](http://editorconfig.org/)
* [Swagger](https://swagger.io/)

## Licence

MIT License

Copyright (c) 2018 Arnaud Raulet

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
