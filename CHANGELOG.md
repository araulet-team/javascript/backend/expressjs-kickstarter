# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

<!--- next entry here -->

## 0.5.2
2020-09-01

### Fixes

- **ci:** artifacts names (9fee1a7fe1185a10a2558c2cfcd41d57a5eca654)

## 0.5.1
2020-08-30

### Fixes

- **folder:** utils (aca1d63ce7b51919e6c27ebcdb60959f88cf68a1)

## 0.5.0
2020-08-30

### Features

- **logging:** add efk stack (98d0c3e457766a20beaaa6507c2e755d3cdac80d)

## 0.4.3
2020-08-29

### Fixes

- **ci:** upgrade gitlab-ci (1dd149b74c44e5d01c24b2fa46c07d7f797ce151)

## 0.4.2
2019-11-09

### Fixes

- **swagger:** change port and path to make it work with redoc (54b22178494eace848f31716ffda9a519cc51eb1)

## 0.4.1
2019-11-09

### Fixes

- **security:** npm audit fix (493ffda93a17b651e3f181747140e89e955736f2)

## 0.4.0
2019-09-03

### Features

- **gitlab-ci:** add jobs from auto-devops (d47672dc61b8e7a2315560f545e2e0be532ffcab)

## 0.3.0
2019-09-01

### Features

- **gitlab-ci.yml:** improve ci (5c38e904f5df00e3af5123ad1f85817adbc0f3e6)

## [1.3.0] - 2019-07-27
### Added
- upgrade to node 12
- fix environments variable not being passed in the last stage

## [1.2.0] - 2019-05-25
### Added
- add favicon
- fix documentation
- Improve docker-compose
- Improve gitlab-ci
- Change application port to 80 instead 3000
- update packages
- healthcheck route
- integration tests

## [1.1.0] - 2019-04-14
### Added
 - Improve Dockerfile and docker-compose.yml for local development

## [1.0.0] - 2018-07-12
### Added
 - init boilerplate
