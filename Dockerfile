# -------------------------------
# build step for npm dependencies
# -------------------------------
FROM node:12-alpine AS builder

ARG NODE_ENV=${NODE_ENV:-production}
ARG NPM_CONFIG_LOGLEVEL=${NPM_CONFIG_LOGLEVEL:-warn}

# Install build toolchain, install node deps and compile native add-ons
RUN apk add --no-cache --virtual .gyp python make g++

RUN mkdir /usr/src/ && chown node:node /usr/src/
WORKDIR /usr/src/

## install dependencies
USER node
COPY package*.json ./
RUN npm ci

# -------------------------------
# base default image for shared
# envs
# -------------------------------
FROM node:12-alpine AS base

ARG PORT=3000
ARG NODE_ENV=${NODE_ENV}
ARG NPM_CONFIG_LOGLEVEL=${NPM_CONFIG_LOGLEVEL}

ENV PORT=$PORT
ENV NPM_CONFIG_LOGLEVEL=${NPM_CONFIG_LOGLEVEL}
ENV NODE_ENV=${NODE_ENV}
ENV PATH /usr/src/node_modules/.bin:$PATH

# Copy built node modules and binaries without including the toolchain
COPY --from=builder /usr/src/node_modules/ /usr/src/node_modules/

USER node

# check every 30s to ensure this service returns HTTP 200
HEALTHCHECK --interval=30s CMD node healthcheck.js

WORKDIR /usr/src/app/
COPY . .

# -------------------------------
# release image (default build on your ci)
# -------------------------------
FROM base AS release

EXPOSE $PORT

CMD ["node", "bin/www"]

# -------------------------------
# development image (--target=develoment)
# -------------------------------
FROM base AS development

EXPOSE $PORT 9229

CMD ["node", "bin/www"]

# -------------------------------
# by default return release one
# -------------------------------
FROM release
