/** @module controllers/root */
'use strict'

/**
 * root api
 * @param  {Object} req
 * @param  {Object} res
 * @return {Object}
 */
function get (req, res) {
  const routes = _listRoutes(req.app)

  res.status(200).json(routes)
}

/**
 * return list of all registered routes
 * @param  {Object} app   expressjs app
 * @return {Object[]}
 */
function _listRoutes (app) {
  return app._router.stack

  .filter(r => r.route)

  .map(r => (
    {
      method: r.route.stack[0].method.toUpperCase(),
      endpoint: r.route.path
    }
  ))
}

module.exports = {
  get
}
