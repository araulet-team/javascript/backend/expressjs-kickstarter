/** @module controllers/users */
'use strict'

const winston = require('winston')
const logger = winston.loggers.get('controllers')

const users = [
  {
    uuid: '1',
    name: 'john'
  },
  {
    uuid: '2',
    name: 'jane'
  },
  {
    uuid: '3',
    name: 'joe'
  }
]

/**
 * return a collection of users from 0 to n
 * @param  {Number} uuid      user uuid's
 * @return {Object[]}         users collection
 */
function _findOne (uuid) {
  const user = users.filter(user => parseInt(user.uuid) === parseInt(uuid))

  return user
}

/**
 * return all users
 * @return {Object[]}      users
 */
function _findAll () {
  return users
}

/**
 * entry endpoint
 * @param  {Object} req
 * @param  {Object} res
 * @return {Object}
 */
function get (req, res) {
  const uuid = req.params.uuid
  const result = uuid ? _findOne(uuid) : _findAll()

  logger.debug(`result return ${JSON.stringify(result)}`)

  res.status(200).json(result)
}

module.exports = {
  get
}

/* istanbul ignore else */
if (process.env.NODE_ENV === 'test') {
  module.exports._findAll = _findAll
  module.exports._findOne = _findOne
}
