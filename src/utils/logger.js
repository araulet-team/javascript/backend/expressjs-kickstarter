/** @module src/utils/logger */
'use strict'

const winston = require('winston')
const { format } = winston
const { combine, label, json, timestamp } = format

function defaultFormat (name) {
  name = name || 'default'

  return combine(
    label({ label: name }),
    timestamp(),
    json()
  )
}

/**
 * [reminder] by default levels are set like this
 * {
 *   error: 0,
 *   warn: 1,
 *   info: 2,
 *   verbose: 3,
 *   debug: 4,
 *   silly: 5
 * }
 *
 * https://github.com/winstonjs/winston#working-with-multiple-loggers-in-winston
 * You can use logger like this in your application
 *
 * const winston = require('winston')
 * const category1 = winston.loggers.get('category1')
 * const category2 = winston.loggers.get('category2')
 *
 * category1.info('hello')
 * category2.info('hello')
 *
 * how Winston loggers are configured
 * winston.loggers.add('category1', {
 *  format: combine(
 *    label({ label: 'category one' }),
 *    json()
 *  ),
 *  transports: [
 *    new winston.transports.Console({ level: 'silly' }),
 *    new winston.transports.File({ filename: 'somefile.log' })
 *  ]
 * });
 *
 * @param  {Object.<transports: Object, loggers: Object>} options
 * @return {<Object>} winston instance
 */
function createLoggers (options) {
  const loggers = _buildLoggersOptions(options)

  loggers.forEach(logger => {
    winston.loggers.add(logger.name, logger.config)
  })

  return winston
}

/**
 * return an array of logger with their full configuration
 * example of logger
 * [
 *   {
 *     name: 'category1',
 *     config: {
 *       transports: [{
 *         console: {
 *           level: 'silly',
 *           colorize: true,
 *           label: 'category one'
 *         },
 *         file: {
 *           filename: '/path/to/some/file'
 *         }
 *       }],
 *       format: [...]
 *   })
 * ]
 * @param  {Object.<transports: Object, loggers: Object>} options
 * @return {Array.<Object>}
 */
function _buildLoggersOptions (options) {
  const loggersList = options.loggers
  const transportsList = options.transports

  // building loggers
  return loggersList.reduce((acc, logger) => {
    // building transport options
    const transports = Object.keys(transportsList)
      .filter(key => logger.transports.indexOf(key) > -1)
      .reduce((acc, key) => {
        // build logger options
        let loggerOpts = Object.assign({}, transportsList[key], logger.options)

        // choose constructor
        if (key === 'console') {
          acc.push(new winston.transports.Console(loggerOpts))
        } else if (key === 'file') {
          acc.push(new winston.transports.File(loggerOpts))
        } else {
          throw Error(`no transports found with name ${key} [need implementation]`)
        }

        return acc
      }, [])

    // building format
    const format = options.format || defaultFormat(logger.options.label)
    const config = Object.assign({}, { transports, format })

    acc.push({
      name: logger.name,
      config
    })

    return acc
  }, [])
}

module.exports = createLoggers
