'use strict'

const _root = require('../../controllers/root')

/**
 * @swagger
 *
 * definitions:
 *   Debug:
 *     type: object
 *     required:
 *       - method
 *       - endpoint
 *     properties:
 *       method:
 *         type: string
 *       endpoint:
 *         type: string
 */

const rootRte = [
  /**
   * @swagger
   * /:
   *   get:
   *     summary: Discovery endpoint [debug]
   *     description: Discovery endpoint [debug]
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     tags:
   *       - discovery
   *     responses:
   *       200:
   *         description: Discovery endpoint [debug] and dependencies
   *         schema:
   *           type: array
   *           items:
   *             $ref: '#/definitions/Debug'
   */
  {
    url: '',
    method: 'get',
    handler: _root.get
  }
]

module.exports = rootRte
