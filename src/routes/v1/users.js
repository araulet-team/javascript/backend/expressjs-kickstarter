/** @module routes/v1/users */
'use strict'

const Joi = require('joi')
const users = require('../../controllers/users')

const errorSchema = {
  statusCode: Joi.number().required(),
  error: Joi.string().required(),
  message: Joi.string().required()
}

/**
 * @swagger
 *
 * definitions:
 *   User:
 *     type: object
 *     required:
 *       - uuid
 *       - name
 *     properties:
 *       uuid:
 *         type: integer
 *       name:
 *         type: string
 *
 *   Error:
 *     type: object
 *     required:
 *       - statusCode
 *       - error
 *       - message
 *     properties:
 *       statusCode:
 *         type: integer
 *       error:
 *         type: string
 *       message:
 *         type: string
 */

const usersRte = [
  /**
   * @swagger
   * /users/:
   *   get:
   *     summary: list of users
   *     description: list of users
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     tags:
   *       - users
   *     responses:
   *       200:
   *         description: get a user
   *         schema:
   *           type: array
   *           items:
   *             $ref: '#/definitions/User'
   */
  {
    url: '/users/',
    method: 'get',
    handler: users.get,
    response: {
      status: {
        200: Joi.array().required().items(
          Joi.object().keys({
            name: Joi.string(),
            uuid: Joi.string()
          })
        ).description('collection of users'),
        400: Joi.object().keys(errorSchema),
        404: Joi.object().keys(errorSchema)
      }
    }
  },

  /**
   * @swagger
   * /users/uuid:
   *   get:
   *     summary: get a user
   *     description: get a user
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     tags:
   *       - users
   *     parameters:
   *       - in: path
   *         name: uuid
   *         type: integer
   *         required: true
   *     responses:
   *       200:
   *         description: get a user
   *         schema:
   *           type: array
   *           items:
   *             $ref: '#/definitions/User'
   *       404:
   *         description: user not found
   *         schema:
   *           $ref: '#/definitions/Error'
   *       400:
   *         description: bad request
   *         schema:
   *           $ref: '#/definitions/Error'
   */
  {
    url: '/users/:uuid/',
    method: 'get',
    handler: users.get,
    validate: {
      params: {
        uuid: Joi.number()
                 .required()
                 .label('User ID')
                 .error(new Error('User ID needs to be a number.'))
      }
    },
    response: {
      status: {
        200: Joi.array().required().items(
          Joi.object().keys({
            name: Joi.string(),
            uuid: Joi.string()
          })
        ),
        400: Joi.object().keys(errorSchema),
        404: Joi.object().keys(errorSchema)
      }
    }
  }
]

module.exports = usersRte
