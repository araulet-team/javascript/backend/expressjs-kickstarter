/** @module routes/v1/healthz */
'use strict'

const _healthz = require('../../controllers/healthz')

const healthzRte = [
  /**
   * @swagger
   * /healthz/:
   *   get:
   *     summary: Application healthcheck
   *     description: Application healthcheck
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     tags:
   *       - healthcheck
   *     responses:
   *       200:
   *         description: Application healthcheck and dependencies
   *         schema:
   *           type: object
   */
  {
    url: '/healthz/',
    method: 'get',
    handler: _healthz.get
  }
]

module.exports = healthzRte
