'use strict'

const Boom = require('boom')
const express = require('express')
const morgan = require('morgan')
const debug = require('debug')('app:bootstrap')
const compression = require('compression')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const path = require('path')
const joi4express = require('joi4express')
const loadRoutes = require('expressjs-routes-loader')()
const loadConfiguration = require('simple-nconf-loader')

const app = express()

debug('start')

debug(`////////// Running environment set to "${process.env.NODE_ENV}" ////////////////`)

// [start] setup configuration
// accessible through req.app.get('config').get('your property')
// -----------------------------------------------------------------------------
debug('setup configuration for expressjs app')
const config = loadConfiguration(path.join(__dirname, 'config'), {})
debug(config.get())

app.set('config', config)

// [start] setup expressjs security
// see https://expressjs.com/en/advanced/best-practice-security.html
// -----------------------------------------------------------------------------
app.use(helmet())

// [start] setup middleware
// https://expressjs.com/en/advanced/best-practice-performance.html
// -----------------------------------------------------------------------------
debug('setup middlewares:body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

debug('setup middlewares:compression')
app.use(compression())

// [start] winston logger
// -----------------------------------------------------------------------------
debug('setup winston loggers')
const logger = require('./src/utils/logger')(config.get('modules:winston'))

// [start] request logger middleware
// -----------------------------------------------------------------------------
debug('setup request logger with morgan')

// do not enabled morgan during tests
/* istanbul ignore if */
if (process.env.NODE_ENV !== 'test') {
  app.use(morgan('combined'))
}

// [start] setup routes with joi validation
// see https://github.com/hapijs/joi
// -----------------------------------------------------------------------------
debug('setup routes')
let routes = loadRoutes([path.join(__dirname, 'src', 'routes'), 'api', 'v1'])
debug(routes)

routes.forEach((route) => {
  app[route.method](route.url, joi4express(route))
})

// only for testing purpose
/* istanbul ignore else */
if (process.env.NODE_ENV === 'test') {
  app.use('/boom', (req, res, next) => {
    throw new Error('Boom!!!')
  })
}

// [start] setup documentation with swagger
// ----------------------------------------------------------------------------
debug('setup documentation')
const swaggerSpec = require(path.join(__dirname, 'swagger'))
                    .generate(routes.map(rte => rte.__file__))

app.get('/swagger.json', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  res.send(swaggerSpec)
})

app.get('/api/docs', (req, res) => {
  res.sendFile(path.join(__dirname, 'swagger', 'redoc.html'))
})

// [start] setup error handler
// all error accross the application should be Boomify
// -----------------------------------------------------------------------------
app.use((err, req, res, next) => {
  /* istanbul ignore else */
  if (!err || !err.isBoom) {
    Boom.boomify(err, { statusCode: err.statusCode || err.status || 500 })
  }

  /* istanbul ignore else */
  if (err.isServer) {
    debug(err)
  }

  logger.error(`${err.output.statusCode} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`)

  res.status(err.output.statusCode).json(err.output.payload)
})

debug('finish')

module.exports = app
