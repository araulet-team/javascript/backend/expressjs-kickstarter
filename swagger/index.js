/** @module swagger/index */
'use strict'

const path = require('path')
const swaggerJSDoc = require('swagger-jsdoc')
const swaggerDefinition = require(path.join(__dirname, 'swaggerDefinition.json'))

/**
 * generate swagger documentation
 * @return {Object} swagger specs
 */
function generate (routes) {
  const options = {
    swaggerDefinition,
    apis: [...new Set(routes)]
  }

  const swaggerSpec = swaggerJSDoc(options)

  return swaggerSpec
}

module.exports = {
  generate
}
